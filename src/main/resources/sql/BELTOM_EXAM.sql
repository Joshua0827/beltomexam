CREATE USER 'joshua'@'localhost' IDENTIFIED BY 'joshua';
CREATE DATABASE beltom_exam;
GRANT ALL PRIVILEGES ON beltom_exam.* TO 'joshua'@'localhost';

USE beltom_exam;

DROP TABLE IF EXISTS tb_user;

CREATE TABLE tb_user(
	id BIGINT(19) UNSIGNED NOT NULL AUTO_INCREMENT,
	name VARCHAR(20) NOT NULL,
	password VARCHAR(20) NOT NULL,
	email VARCHAR(255),
	create_date DATETIME,
	PRIMARY KEY(id),
	UNIQUE(name)
);

INSERT INTO tb_user(name, password, create_date) VALUES
('joshua', 'joshua', NOW()),
('admin', 'admin', NOW()),
('junit', 'junit', NOW());

DROP TABLE IF EXISTS tb_shipment_d;
DROP TABLE IF EXISTS tb_shipment_m;

CREATE TABLE tb_shipment_m(
	shipment_id VARCHAR(22) NOT NULL,
	shipment_name VARCHAR(40) NOT NULL,
	shipment_create_time DATETIME,
	receiver VARCHAR(20),
	place VARCHAR(20),
	email VARCHAR(50),
	phone VARCHAR(20),
	status VARCHAR(1) NOT NULL DEFAULT '0',
	remark TEXT,
	op_emp VARCHAR(20),
	plan_shipment_time DATETIME,
	start_shipment_time DATETIME,
	finish_shipment_time DATETIME,
	PRIMARY KEY(shipment_id)
);

CREATE TABLE tb_shipment_d(
	order_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	product_name VARCHAR(40) NOT NULL,
	order_num INT(11) UNSIGNED NOT NULL,
	shipment_id VARCHAR(22) NOT NULL,
	PRIMARY KEY(order_id),
	FOREIGN KEY(shipment_id)
		REFERENCES tb_shipment_m(shipment_id)
);
DROP TABLE IF EXISTS tb_shipment_history;

CREATE TABLE tb_shipment_history(
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	shipment_id VARCHAR(22) NOT NULL,
	status VARCHAR(1) NOT NULL,
	create_date DATETIME,
	PRIMARY KEY(id)
);

