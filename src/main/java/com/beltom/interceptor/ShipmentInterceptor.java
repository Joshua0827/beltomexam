package com.beltom.interceptor;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class ShipmentInterceptor extends AbstractInterceptor {
	private static final long serialVersionUID = -5313088908196522439L;

	@Override
	/*
	 * 過濾Session，檢查使用者是否已登入
	 */
	public String intercept(ActionInvocation invocation) throws Exception {
		String result = ActionSupport.ERROR;
		
		if(ActionContext.getContext().getSession().containsKey("userInfo")) {
			result = invocation.invoke();
		}

		return result;
	}

}
