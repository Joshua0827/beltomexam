/**
 * ShipmentCancelServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.beltom.axis;

public class ShipmentCancelServiceServiceLocator extends org.apache.axis.client.Service
		implements com.beltom.axis.ShipmentCancelServiceService {
	private static final long serialVersionUID = -5524452502244529907L;

	public ShipmentCancelServiceServiceLocator() {
	}

	public ShipmentCancelServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
		super(config);
	}

	public ShipmentCancelServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName)
			throws javax.xml.rpc.ServiceException {
		super(wsdlLoc, sName);
	}

	// Use to get a proxy class for ShipmentCancelService
	private java.lang.String ShipmentCancelService_address = "http://localhost:8080/BeltomExamWebService/services/ShipmentCancelService";

	public java.lang.String getShipmentCancelServiceAddress() {
		return ShipmentCancelService_address;
	}

	// The WSDD service name defaults to the port name.
	private java.lang.String ShipmentCancelServiceWSDDServiceName = "ShipmentCancelService";

	public java.lang.String getShipmentCancelServiceWSDDServiceName() {
		return ShipmentCancelServiceWSDDServiceName;
	}

	public void setShipmentCancelServiceWSDDServiceName(java.lang.String name) {
		ShipmentCancelServiceWSDDServiceName = name;
	}

	public com.beltom.axis.ShipmentCancelService getShipmentCancelService() throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(ShipmentCancelService_address);
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getShipmentCancelService(endpoint);
	}

	public com.beltom.axis.ShipmentCancelService getShipmentCancelService(java.net.URL portAddress)
			throws javax.xml.rpc.ServiceException {
		try {
			com.beltom.axis.ShipmentCancelServiceSoapBindingStub _stub = new com.beltom.axis.ShipmentCancelServiceSoapBindingStub(
					portAddress, this);
			_stub.setPortName(getShipmentCancelServiceWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	public void setShipmentCancelServiceEndpointAddress(java.lang.String address) {
		ShipmentCancelService_address = address;
	}

	/**
	 * For the given interface, get the stub implementation. If this service has no
	 * port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		try {
			if (com.beltom.axis.ShipmentCancelService.class.isAssignableFrom(serviceEndpointInterface)) {
				com.beltom.axis.ShipmentCancelServiceSoapBindingStub _stub = new com.beltom.axis.ShipmentCancelServiceSoapBindingStub(
						new java.net.URL(ShipmentCancelService_address), this);
				_stub.setPortName(getShipmentCancelServiceWSDDServiceName());
				return _stub;
			}
		} catch (java.lang.Throwable t) {
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "
				+ (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
	}

	/**
	 * For the given interface, get the stub implementation. If this service has no
	 * port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface)
			throws javax.xml.rpc.ServiceException {
		if (portName == null) {
			return getPort(serviceEndpointInterface);
		}
		java.lang.String inputPortName = portName.getLocalPart();
		if ("ShipmentCancelService".equals(inputPortName)) {
			return getShipmentCancelService();
		} else {
			java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	public javax.xml.namespace.QName getServiceName() {
		return new javax.xml.namespace.QName("http://axis.beltom.com", "ShipmentCancelServiceService");
	}

	private java.util.HashSet ports = null;

	public java.util.Iterator getPorts() {
		if (ports == null) {
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName("http://axis.beltom.com", "ShipmentCancelService"));
		}
		return ports.iterator();
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(java.lang.String portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {

		if ("ShipmentCancelService".equals(portName)) {
			setShipmentCancelServiceEndpointAddress(address);
		} else { // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {
		setEndpointAddress(portName.getLocalPart(), address);
	}

}
