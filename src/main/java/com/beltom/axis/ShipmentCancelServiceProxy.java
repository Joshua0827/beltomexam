package com.beltom.axis;

public class ShipmentCancelServiceProxy implements com.beltom.axis.ShipmentCancelService {
	private String _endpoint = null;
	private com.beltom.axis.ShipmentCancelService shipmentCancelService = null;

	public ShipmentCancelServiceProxy() {
		_initShipmentCancelServiceProxy();
	}

	public ShipmentCancelServiceProxy(String endpoint) {
		_endpoint = endpoint;
		_initShipmentCancelServiceProxy();
	}

	private void _initShipmentCancelServiceProxy() {
		try {
			shipmentCancelService = (new com.beltom.axis.ShipmentCancelServiceServiceLocator())
					.getShipmentCancelService();
			if (shipmentCancelService != null) {
				if (_endpoint != null)
					((javax.xml.rpc.Stub) shipmentCancelService)._setProperty("javax.xml.rpc.service.endpoint.address",
							_endpoint);
				else
					_endpoint = (String) ((javax.xml.rpc.Stub) shipmentCancelService)
							._getProperty("javax.xml.rpc.service.endpoint.address");
			}

		} catch (javax.xml.rpc.ServiceException serviceException) {
		}
	}

	public String getEndpoint() {
		return _endpoint;
	}

	public void setEndpoint(String endpoint) {
		_endpoint = endpoint;
		if (shipmentCancelService != null)
			((javax.xml.rpc.Stub) shipmentCancelService)._setProperty("javax.xml.rpc.service.endpoint.address",
					_endpoint);

	}

	public com.beltom.axis.ShipmentCancelService getShipmentCancelService() {
		if (shipmentCancelService == null)
			_initShipmentCancelServiceProxy();
		return shipmentCancelService;
	}

	public java.lang.String cancelShipment(java.lang.String shipmentId) throws java.rmi.RemoteException {
		if (shipmentCancelService == null)
			_initShipmentCancelServiceProxy();
		return shipmentCancelService.cancelShipment(shipmentId);
	}
}