package com.beltom.model;

import static com.beltom.util.StringUtil.nl;

import java.io.Serializable;

public class ShipmentDetail implements Serializable {
	private static final long serialVersionUID = 1615853175340563432L;
	
	private Integer orderId;
	private String productName;
	private Integer orderQuantity;
	private String shipmentId;
	
	public Integer getOrderId() {
		return orderId;
	}
	
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public Integer getOrderQuantity() {
		return orderQuantity;
	}
	
	public void setOrderQuantity(Integer orderQuantity) {
		this.orderQuantity = orderQuantity;
	}
	
	public String getShipmentId() {
		return shipmentId;
	}
	
	public void setShipmentId(String shipmentId) {
		this.shipmentId = shipmentId;
	}
	
	public String toString() {
		StringBuilder detail = new StringBuilder();
		
		detail.append("orderId:").append(orderId).append(nl);
		detail.append("productName:").append(productName).append(nl);
		detail.append("shipmentId:").append(shipmentId).append(nl);
		detail.append("orderQuantity").append(orderQuantity);

		return detail.toString();
	}
}
