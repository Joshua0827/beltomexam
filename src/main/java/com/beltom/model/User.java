package com.beltom.model;

import static com.beltom.util.StringUtil.nl;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private static final long serialVersionUID = 269446292083043913L;
	
	private Long id;
	private String name;
	private String password;
	private String email;
	private Date createDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public String toString() {
		StringBuilder result = new StringBuilder();
		
		result.append("id:").append(id).append(nl);
		result.append("name:").append(name).append(nl);
		result.append("email:").append(email).append(nl);
		result.append("createDate:").append(createDate).append(nl);
		
		return result.toString();
	}
}
