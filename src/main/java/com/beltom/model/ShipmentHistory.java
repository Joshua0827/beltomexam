package com.beltom.model;

import static com.beltom.util.StringUtil.nl;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

public class ShipmentHistory implements Serializable {
	private static final long serialVersionUID = 7816737935745096157L;
	
	private Integer id;
	private String shipmentId;
	private String status;
	private String stateString;
	private Date createDate;
	
	private static Map<String, String> state = null;
	
	static {
		state = new Hashtable<>();
		
		state.put("0", "草稿");
		state.put("1", "暫停");
		state.put("2", "可出貨");
		state.put("3", "出貨中");
		state.put("4", "出貨完成");
		state.put("5", "已取消");
	}

	public String getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(String shipmentId) {
		this.shipmentId = shipmentId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStateString() {
		stateString = state.get(status);
		
		return stateString;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String toString() {
		StringBuilder detail = new StringBuilder();
		
		detail.append("id:").append(id).append(nl);
		detail.append("shipmentId:").append(shipmentId).append(nl);
		detail.append("status:").append(status).append(nl);
		detail.append("createDate:").append(createDate);
		
		return detail.toString();
	}
}
