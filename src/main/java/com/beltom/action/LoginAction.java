package com.beltom.action;

import static com.beltom.util.StringUtil.nl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.beltom.biz.UserDaoManager;
import com.beltom.model.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport {
	private static final long serialVersionUID = -1361921824990703066L;
	private static final Logger log = LogManager.getLogger(LoginAction.class);
	
	private User user;
	
	/*
	 * 登入action
	 */
	public String login() {
		log.debug("user info :{}{}", nl, user.toString());
		
		UserDaoManager userDaoManager = new UserDaoManager();
		
		if(userDaoManager.isValidUser(user)) {
			ActionContext.getContext().getSession().put("userInfo", user.getName());
			
			return SUCCESS;
		}
		
		addFieldError("user.name", "帳號或密碼錯誤");
		
		return INPUT;
	}
	
	/*
	 * 驗證欄位，使用者名稱、密碼不能是空白格、空值
	 */
	public void validate() {
		if(user.getName().trim().isEmpty()) addFieldError("user.name", "請輸入帳號");
		
		if(user.getPassword().trim().isEmpty()) addFieldError("user.password", "請輸入密碼");
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
