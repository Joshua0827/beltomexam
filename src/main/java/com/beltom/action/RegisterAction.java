package com.beltom.action;

import static com.beltom.util.StringUtil.nl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.beltom.biz.UserDaoManager;
import com.beltom.model.User;
import com.opensymphony.xwork2.ActionSupport;

public class RegisterAction extends ActionSupport {
	private static final long serialVersionUID = 8525628726206014444L;
	private static final Logger log = LogManager.getLogger(RegisterAction.class);
	
	private User user;
	private String passwordRepeat;
	
	/*
	 * 註冊action
	 */
	public String register() {
		log.debug("user info : {}{}", nl, user.toString());
		
		UserDaoManager userDaoManager = new UserDaoManager();
		
		if(userDaoManager.isUserExists(user.getName())) {
			addFieldError("user.userName", "此帳號已存在");
			
			return INPUT;
		} else {
			userDaoManager.createUser(user);
		}
		
		return SUCCESS;
	}
	
	/*
	 * 驗證欄位，使用者名稱、密碼不能是空白格、空值
	 * 密碼與重複輸入要一致
	 */
	public void validate() {
		if(user.getName().trim().isEmpty()) addFieldError("user.name", "請輸入帳號");
		
		if(user.getPassword().trim().isEmpty()) addFieldError("user.password", "請輸入密碼");
		
		if(passwordRepeat.trim().isEmpty()) addFieldError("passwordRepeat", "請重複密碼");
		
		if(!user.getPassword().equals(passwordRepeat)) addFieldError("user.userPassword", "密碼不一致");
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}
}
