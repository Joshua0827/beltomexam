package com.beltom.action;

import static com.beltom.util.StringUtil.nl;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.beltom.axis.ShipmentCancelService;
import com.beltom.axis.ShipmentCancelServiceProxy;
import com.beltom.biz.ShipmentDaoManager;
import com.beltom.biz.ShipmentDetailDaoManager;
import com.beltom.dao.ShipmentHistoryDao;
import com.beltom.model.Shipment;
import com.beltom.model.ShipmentDetail;
import com.beltom.model.ShipmentHistory;
import com.beltom.util.DateUtil;
import com.opensymphony.xwork2.ActionSupport;

public class ShipmentAction extends ActionSupport {
	private static final long serialVersionUID = 8418777122349709368L;
	private static final Logger log = LogManager.getLogger(ShipmentAction.class);
	
	private Shipment shipment;
	private String shipmentCreateTime;
	private String planShipmentTime;
	private String startTime;
	private String endTime;
	private String shipmentDetailXml;
	
	private int pageIndex;
	private int pageSize;
	private int totalRecords;
	
	private List<Shipment> shipments;
	private List<ShipmentDetail> shipmentDetails;
	private List<ShipmentHistory> shipmentHistories;
	
	/*
	 * 出貨作業查詢
	 */
	public String query() {
		log.debug("{}{}startTime:{}{}endTime:{}{}pageIndex:{}{}pageSize:{}", 
				shipment.toString(), nl, startTime, nl, endTime, nl, pageIndex, nl, pageSize);
		
		ShipmentDaoManager manager = new ShipmentDaoManager();
		//查詢符合條件總筆數
		setTotalRecords(manager.getShipmentCount(shipment, startTime, endTime));
		
		//總筆數大於零再執行分頁查詢
		if(totalRecords > 0) 
			setShipments(manager.queryShipmentByPage(shipment, startTime, endTime, pageIndex, pageSize));
		
		return SUCCESS;
	}
	
	/*
	 * 出貨作業查詢
	 * 資料欄位驗證
	 */
	public void validateQuery() {
		String pattern = "yyyy/MM/dd";
		
		if(!startTime.isEmpty() || !endTime.isEmpty()) {
			if(!DateUtil.isExistDate(pattern, startTime)) addFieldError("startTime", "起始日期錯誤");
			if(!DateUtil.isExistDate(pattern, endTime)) addFieldError("endTime", "終止日期錯誤");
		}
	}
	
	/*
	 * 出貨作業建立
	 */
	public String create() {
		log.debug("{}{}shipmentDetailXml:{}", shipment.toString(), nl, shipmentDetailXml);
		
		//解析Xml字串
		parseShipmentDetailXml();
		
		//有出貨明細才執行建立出貨單與儲存明細
		if(shipmentDetails.size() > 0) {
			createShipment();
			saveShipmentDetails();
			saveShipmentHistory();
		}
		
		return SUCCESS;
	}
	
	/*
	 * 出貨作業建立
	 * 資料欄位驗證
	 */
	public void validateCreate() {
		String pattern = "yyyy/MM/dd HH:mm:ss";
		
		if(shipment.getShipmentName().trim().isEmpty()) 
			addFieldError("shipment.shipmentName", "請輸入出貨名稱");
		
		if(!planShipmentTime.trim().isEmpty()) {
			if(!DateUtil.isExistDate(pattern, planShipmentTime)) addFieldError("planShipmentTime", "預計出貨時間錯誤");
		}
		
		if(shipment.getPlace().trim().isEmpty())
			addFieldError("shipment.place", "請輸入出貨地點");
		
		if(shipment.getReceiver().trim().isEmpty())
			addFieldError("shipment.receiver", "請輸入收件人");
		
		if(shipment.getPhone().trim().isEmpty())
			addFieldError("shipment.phone", "請輸入收件人電話");
		
		if(shipment.getOperator().trim().isEmpty())
			addFieldError("shipment.operator", "請輸入出貨人員");
		
		if(shipmentDetailXml.trim().isEmpty())
			addFieldError("shipmentDetail", "請輸入出貨單明細");
	}
	
	/*
	 * 解析客戶端傳來的ShipmentDetailXml字串
	 */
	@SuppressWarnings("unchecked")
	private void parseShipmentDetailXml() {
		shipmentDetails = new ArrayList<>();
		SAXReader reader = new SAXReader();
		
		try {
			Document doc = reader.read(new ByteArrayInputStream(shipmentDetailXml.getBytes("UTF-8")));
			Element root = doc.getRootElement();
			Element element;
			
			for (Iterator<Element> i = (Iterator<Element>)root.elementIterator("product"); i.hasNext();) {
				ShipmentDetail shipmentDetail = new ShipmentDetail();
				element = i.next();
				shipmentDetail.setProductName(element.elementText("name"));
				shipmentDetail.setOrderQuantity(Integer.parseInt(element.elementText("qty")));
				shipmentDetails.add(shipmentDetail);
			}
		} catch (UnsupportedEncodingException | DocumentException e) {
			log.error("Exception : ", e);
		}
	}
	
	/*
	 * 建立出貨單
	 */
	private void createShipment() {
		ShipmentDaoManager manager = new ShipmentDaoManager();
		shipment.setStatus("0");
		shipment.setPlanShipmentTime(DateUtil.parse("yyyy/MM/dd HH:mm:ss", planShipmentTime));
		shipment.setShipmentCreateTime(new Date());
		manager.createShipment(shipment);
	}
	
	/*
	 * 儲存出貨單明細
	 */
	private void saveShipmentDetails() {
		for(ShipmentDetail shipmentDetail : shipmentDetails) {
			shipmentDetail.setShipmentId(shipment.getShipmentId());
			ShipmentDetailDaoManager manager = new ShipmentDetailDaoManager();
			
			manager.createShipmentDetail(shipmentDetail);
		}
	}
	
	/*
	 * 儲存出貨單歷程
	 */
	private void saveShipmentHistory() {
		ShipmentHistory shipmentHistory = new ShipmentHistory();
		shipmentHistory.setShipmentId(shipment.getShipmentId());
		shipmentHistory.setStatus(shipment.getStatus());
		shipmentHistory.setCreateDate(new Date());
		
		ShipmentHistoryDao dao = new ShipmentHistoryDao();
		dao.saveShipmentHistory(shipmentHistory);
	}
	
	public String update() {
		log.debug(shipment.toString());
		
		ShipmentDaoManager shipmentDaoManager = new ShipmentDaoManager();
		setShipment(shipmentDaoManager.getShipmentById(shipment.getShipmentId()));
		ShipmentDetailDaoManager manager = new ShipmentDetailDaoManager();
		setShipmentDetails(manager.getShipmentDetailsByShipmentId(shipment.getShipmentId()));
		
		return SUCCESS;
	}
	
	public String updateShipment() {
		log.debug("{}{}shipmentDetailXml:{}", shipment.toString(), nl, shipmentDetailXml);
		
		//解析Xml字串
		parseShipmentDetailXml();
		
		//有出貨明細才執行建立出貨單與儲存明細
		if(shipmentDetails.size() > 0) {
			updateShipmentData();
			deleteShipmentDetails();
			saveShipmentDetails();
		}
		
		return SUCCESS;
	}
	
	private void updateShipmentData() {
		ShipmentDaoManager manager = new ShipmentDaoManager();
		shipment.setPlanShipmentTime(DateUtil.parse("yyyy/MM/dd HH:mm:ss", planShipmentTime));
		shipment.setShipmentCreateTime(DateUtil.parse("yyyy/MM/dd HH:mm:ss", shipmentCreateTime));
		manager.updateShipment(shipment);
	}
	
	private void deleteShipmentDetails() {
		ShipmentDetailDaoManager manager = new ShipmentDetailDaoManager();
		manager.deleteShipmentDetailsByShipmentId(shipment.getShipmentId());
	}
	
	public String queryDetail() {
		log.debug(shipment.toString());
		
		ShipmentDetailDaoManager manager = new ShipmentDetailDaoManager();
		setShipmentDetails(manager.getShipmentDetailsByShipmentId(shipment.getShipmentId()));
		
		return SUCCESS;
	}
	
	public String startShipment() {
		log.debug(shipment.toString());
		
		updateShipmentStatus("2");
		
		return SUCCESS;
	}
	
	public String pauseShipment() {
		log.debug(shipment.toString());
		
		updateShipmentStatus("1");
		
		return SUCCESS;
	}
	
	private void updateShipmentStatus(String status) {
		ShipmentDaoManager manager = new ShipmentDaoManager();
		shipment = manager.getShipmentById(shipment.getShipmentId());
		shipment.setStatus(status);
		manager.updateShipment(shipment);
		saveShipmentHistory();
	}
	
	public String cancelShipment() {
		log.debug(shipment.toString());
		
		ShipmentCancelService service = new ShipmentCancelServiceProxy();
		String result = "fail";
		
		try {
			result = service.cancelShipment(shipment.getShipmentId());
		} catch (RemoteException e) {
			log.error("Exception:", e);
		}
		
		return result;
	}
	
	/*
	 * 出貨作業歷程查詢
	 */
	public String queryHistory() {
		log.debug(shipment.toString());
		
		ShipmentHistoryDao dao = new ShipmentHistoryDao();
		setShipmentHistories(dao.queryHistoryByShipmentId(shipment.getShipmentId()));
		
		return SUCCESS;
	}
	
	/*
	 * 出貨作業修改
	 * 資料欄位驗證
	 */
	public void validateShipmentUpdate() {
		if(shipment.getShipmentId().trim().isEmpty()) addFieldError("shipment.shipmentId", "請輸入出貨單號");
			
		validateCreate();
	}

	public Shipment getShipment() {
		return shipment;
	}

	public void setShipment(Shipment shipment) {
		this.shipment = shipment;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getShipmentDetailXml() {
		return shipmentDetailXml;
	}

	public void setShipmentDetailXml(String shipmentDetailXml) {
		this.shipmentDetailXml = shipmentDetailXml;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public List<Shipment> getShipments() {
		return shipments;
	}

	public void setShipments(List<Shipment> shipments) {
		this.shipments = shipments;
	}

	public List<ShipmentDetail> getShipmentDetails() {
		return shipmentDetails;
	}

	public void setShipmentDetails(List<ShipmentDetail> shipmentDetails) {
		this.shipmentDetails = shipmentDetails;
	}

	public List<ShipmentHistory> getShipmentHistories() {
		return shipmentHistories;
	}

	public void setShipmentHistories(List<ShipmentHistory> shipmentHistories) {
		this.shipmentHistories = shipmentHistories;
	}

	public String getPlanShipmentTime() {
		return planShipmentTime;
	}

	public void setPlanShipmentTime(String planShipmentTime) {
		this.planShipmentTime = planShipmentTime;
	}

	public String getShipmentCreateTime() {
		return shipmentCreateTime;
	}

	public void setShipmentCreateTime(String shipmentCreateTime) {
		this.shipmentCreateTime = shipmentCreateTime;
	}
}
