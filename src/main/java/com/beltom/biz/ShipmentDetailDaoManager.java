package com.beltom.biz;

import java.util.List;

import com.beltom.dao.ShipmentDetailDao;
import com.beltom.model.ShipmentDetail;

/*
 * ProductShipmentDao操作相關的商業邏輯
 */
public class ShipmentDetailDaoManager {
	ShipmentDetailDao dao = new ShipmentDetailDao();
	
	/*
	 * 依照出貨單號搜尋產品清單
	 */
	public List<ShipmentDetail> getShipmentDetailsByShipmentId(String shipmentId) {
		return dao.queryShipmentDetailsByShipmentId(shipmentId);
	}
	
	/*
	 * 依出貨單號刪除產品清單
	 */
	public void deleteShipmentDetailsByShipmentId(String shipmentId) {
		dao.deleteShipmentDetailsByShipmentId(shipmentId);
	}
	
	/*
	 * 新增產品清單
	 */
	public void createShipmentDetail(ShipmentDetail shipmentDetail) {
		dao.saveShipmentDetail(shipmentDetail);
	}
	
	/*
	 * 修改產品清單明細
	 */
	public void updateShipmentDetail(ShipmentDetail shipmentDetail) {
		dao.updateShipmentDetail(shipmentDetail);
	}
	
	/*
	 * 刪除產品清單
	 */
	public void deleteShipmentDetail(ShipmentDetail shipmentDetail) {
		dao.deleteShipmentDetail(shipmentDetail);
	}
}
