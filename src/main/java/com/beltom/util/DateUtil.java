package com.beltom.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	public static final boolean isExistDate(String pattern, String dateString) {
		return (parse(pattern, dateString) != null);
	}
	
	public static final Date parse(String pattern, String dateString) {
		Date date = null;
		
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			formatter.setLenient(false);
			
			date = formatter.parse(dateString);
		} catch(ParseException e) {}
		
		return date;
	}
	
	public static final String format(String pattern, Date dateObj) {
		return new SimpleDateFormat(pattern).format(dateObj);
	}
}
