<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<title>Beltom - Login</title>
</head>
<body>
<div class="container">
  <h1>Beltom</h1>
  <s:fielderror />
  <form action="login" method="post" class="needs-validation" novalidate>
    <div class="form-group">
      <label for="user.name">Username</label>
      <input 
        type="text" class="form-control" 
        id="user.name" name="user.name" 
        placeholder="Enter username" required />
      <div class="invalid-feedback">Please fill out this field.</div>
    </div>
    <div class="form-group">
      <label for="user.password">Password</label>
      <input 
        type="password" class="form-control" 
        id="user.password" name="user.password" 
        placeholder="Enter password" required />
      <div class="invalid-feedback">Please fill out this field.</div>
    </div>
    <button type="submit" class="btn btn-primary">登入</button><br />
    <s:a href="register.jsp" class="text-decoration-none"><i>沒有帳號 - 請按此註冊</i></s:a>
  </form>
</div>
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<div class="container-fluid">
  <h1 class="text-center">Beltom</h1>
  <div class="row h-100 justify-content-center align-items-center">
    <form action="login" method="post" class="needs-validation" novalidate>
      <s:fielderror />
      <div class="form-group">
        <label for="user.name">Username</label>
        <input 
          type="text" class="form-control" 
          id="user.name" name="user.name" 
          placeholder="Enter username" maxlength="20" required />
        <div class="invalid-feedback">Please fill out this field.</div>
      </div>
      <div class="form-group">
        <label for="user.password">Password</label>
        <input 
          type="password" class="form-control" 
          id="user.password" name="user.password" 
          placeholder="Enter password" required />
        <div class="invalid-feedback">Please fill out this field.</div>
      </div>
      <button type="submit" class="btn btn-primary">登入</button><br />
      <s:a href="register.jsp" class="text-decoration-none"><i>沒有帳號 - 請按此註冊</i></s:a>
    </form>
  </div>
</div>
<div class="jumbotron text-center" >
	<form action="login" method="post" onsubmit="return validateForm()">
		<s:fielderror />
		<label for="user.userName">帳號</label>
		<input type="text" id="user.userName" name="user.userName" required />
		<span id="errName"></span><br/>
		<label for="user.userPassword">密碼</label>
		<input type="password" id="user.userPassword" name="user.userPassword" required />
		<span id="errPassword"></span>
		<input type="submit" value="登入" />
	</form>
	<s:a href="register.jsp" class="text-decoration-none"><i>沒有帳號 - 請按此註冊</i></s:a>
</div>
</body>
</html>