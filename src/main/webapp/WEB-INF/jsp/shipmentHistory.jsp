<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Beltom - Shipment History</title>
<link rel="stylesheet" type="text/css" href="css/form.css">
<script type="text/javascript" src="js/shipment.js"></script>
</head>
<body>
<h1><i>Beltom 百通科技</i></h1>
<h2><s:a href="shipment">出貨排程查詢</s:a></h2>
<div>
	<h2>出貨歷程查詢</h2>
	<form id="shipmentForm" method="post">
		<s:fielderror />
		<label for="shipment.shipmentName">出貨名稱</label>
		<input type="text" id="shipment.shipmentName" name="shipment.shipmentName" />
		<label for="startTime">建立時間</label>
		<input type="text" name="startTime" id="startTime" placeholder="起 yyyy/mm/dd" />
		<span id="errStartTime"></span><br/>
		<input type="text" name="endTime" id="endTime" placeholder="迄 yyyy/mm/dd" />
		<span id="errEndTime"></span>
		<input type="button" onclick="queryShipment('shipmentHistoryQuery')" value="查詢" />
	</form>
</div>
</body>
</html>