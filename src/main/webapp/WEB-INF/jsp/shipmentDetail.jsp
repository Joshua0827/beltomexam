<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Beltom - Shipment Detail</title>
<link rel="stylesheet" type="text/css" href="css/shipment_query.css">
<link rel="stylesheet" type="text/css" href="css/align_center.css">
</head>
<body>
<div align="center" style="margin-top: 50px">
	<h2>出貨明細查詢結果</h2>
	<hr />
	<table>
		<thead>
			<tr>
				<td>出貨編號</td>
				<td>物品名稱</td>
				<td>數量</td>
			</tr>
		</thead>
		<tbody>
			<s:iterator value="shipmentDetails">
				<tr>
					<td><s:property value="shipmentId" /></td>
					<td><s:property value="productName" /></td>
					<td><s:property value="orderQuantity" /></td>
				</tr>
			</s:iterator>
		</tbody>
	</table>
</div>
</body>
</html>