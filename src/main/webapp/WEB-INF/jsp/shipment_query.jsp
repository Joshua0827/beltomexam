<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Beltom - Shipment Query</title>
<script type="text/javascript" src="js/shipment_query.js"></script>
<link rel="stylesheet" type="text/css" href="css/shipment_query.css">
<link rel="stylesheet" type="text/css" href="css/align_center.css">
</head>
<body onload="pagination()">
<h1><i>Beltom 百通科技</i></h1>
<div align="center" style="margin-top: 50px">
	<h2>出貨查詢結果</h2>
	<hr />
	<table>
		<thead>
			<tr>
				<td>功能</td>
				<td>出貨編號</td>
				<td>建立時間</td>
				<td>出貨名稱</td>
				<td>預計出貨時間</td>
				<td>出貨執行時間</td>
				<td>出貨完成</td>
				<td>狀態</td>
			</tr>
		</thead>
		<tbody>
			<s:iterator value="shipments" id="sid">
				<tr>
					<td>
						<s:if test='#sid.status.equals("0")'>
							<s:a href="javascript: updateShipment('%{ shipmentId }')">修改</s:a>/
							<s:a href="javascript: startShipment('%{ shipmentId }')">出貨</s:a>/
							<s:a href="javascript: pauseShipment('%{ shipmentId }')">暫停</s:a>
						</s:if>
						<s:elseif test='#sid.status.equals("1")'>
							<s:a href="javascript: updateShipment('%{ shipmentId }')">修改</s:a>/
							<s:a href="javascript: cancelShipment('%{ shipmentId }')">取消</s:a>/
							<s:a href="javascript: startShipment('%{ shipmentId }')">出貨</s:a>
						</s:elseif>
						<s:elseif test='#sid.status.equals("2")'>
							<s:a href="javascript: pauseShipment('%{ shipmentId }')">暫停</s:a>
						</s:elseif>
						<s:elseif test='#sid.status.equals("3")'>
							<s:a href="javascript: showShipmentDetail('%{ shipmentId }')">內容</s:a>
						</s:elseif>
						<s:elseif test='#sid.status.equals("4")'>
							<s:a href="javascript: showShipmentDetail('%{ shipmentId }')">內容</s:a>
						</s:elseif>
						<s:else>
							<s:a href="javascript: showShipmentDetail('%{ shipmentId }')">內容</s:a>
						</s:else>
					</td>
					<td><s:property value="shipmentId" /></td>
					<td><s:date name="shipmentCreateTime" format="yyyy/MM/dd HH:mm:ss" /></td>
					<td><s:property value="shipmentName" /></td>
					<td><s:date name="planShipmentTime" format="yyyy/MM/dd HH:mm:ss" /></td>
					<td><s:date name="startShipmentTime" format="yyyy/MM/dd HH:mm:ss" /></td>
					<td><s:date name="finishShipmentTime" format="yyyy/MM/dd HH:mm:ss" /></td>
					<td><s:property value="stateString" /></td>
				</tr>
			</s:iterator>
			<s:hidden 
				id="shipmentId" 
				name="shipment.shipmentId" 
				value="%{ shipment.shipmentId }" />
			<s:hidden 
				id="shipmentName" 
				name="shipment.shipmentName" 
				value="%{ shipment.shipmentName }" />
			<s:hidden 
				id="status" 
				name="shipment.status" 
				value="%{ shipment.status }" />
			<s:hidden 
				id="startTime" 
				name="startTime" 
				value="%{ startTime }" />
			<s:hidden 
				id="endTime" 
				name="endTime" 
				value="%{ endTime }" />
			<s:hidden 
				id="pageIndex" 
				name="pageIndex" 
				value="%{ pageIndex }" />
			<s:hidden 
				id="pageSize" 
				name="pageSize" 
				value="%{ pageSize }" />
			<s:hidden 
				id="totalRecords" 
				value="%{ totalRecords }" />
		</tbody>
	</table>
	<ul id="pages"></ul>
</div>
</body>
</html>