<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Beltom - Shipment Update</title>
<link rel="stylesheet" type="text/css" href="css/align_center.css">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/shipment_update.js"></script>
</head>
<body>
<h1><i>Beltom 百通科技</i></h1>
<div align="center" style="margin-top: 50px">
	<h2>修改出貨作業</h2>
	<form action="shipmentUpdate" method="post">
		<table>
			<tr>
				<td>出貨編號</td>
				<td>
					<s:property value="%{ shipment.shipmentId }" />
					<s:hidden 
						id="shipment.shipmentId" 
						name="shipment.shipmentId" 
						value="%{ shipment.shipmentId }" />
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>出貨名稱</td>
				<td>
					<s:property value="%{ shipment.shipmentName }" />
					<s:hidden 
						id="shipment.shipmentName" 
						name="shipment.shipmentName" 
						value="%{ shipment.shipmentName }" />
				</td>
				<td>預計出貨時間</td>
				<td>
					<input type="text"
							id="planShipmentTime"
							name="planShipmentTime" 
							placeholder="yyyy/MM/dd HH:mm:ss"
							value="<s:date name='shipment.planShipmentTime' format='yyyy/MM/dd HH:mm:ss' />"
							required />
					<span id="errPlanTime"></span>
				</td>
			</tr>
			<tr>
				<td>出貨地點</td>
				<td>
					<input type="text" 
							id="shipment.place" 
							name="shipment.place" 
							value="${ shipment.place }"
							required />
					<span id="errPlace"></span>
				</td>
				<td>收件人</td>
				<td>
					<input type="text" 
							id="shipment.receiver" 
							name="shipment.receiver" 
							value="${ shipment.receiver }" 
							required />
					<span id="errReceiver"></span>
				</td>
			</tr>
			<tr>
				<td>收件人電話</td>
				<td>
					<input type="text" 
							id="shipment.phone" 
							name="shipment.phone" 
							value="${ shipment.phone }" 
							required />
					<span id="errPhone"></span>
				</td>
				<td>出貨人員</td>
				<td>
					<input type="text" 
							id="shipment.operator" 
							name="shipment.operator" 
							value="${ shipment.operator }"
							required />
					<span id="errOperator"></span>
				</td>
			</tr>
			<tr><td colspan="4">備註</td></tr>
			<tr>
				<td colspan="4">
					<textarea id="shipment.remark" name="shipment.remark">${ shipment.remark }</textarea>
				</td>
			</tr>
		</table>
		<div>
			<span id="errShipmentDetails"></span>
			<table>
				<tr>
					<td>物品名稱</td><td>數量</td><td></td>
				</tr>
				<tr>
					<td><input type="text" id="name" /></td>
					<td><input type="number" id="qty" /></td>
					<td>
						<input id="addRow" type="button" value="新增" />
					</td>
				</tr>
			</table>
		</div>
		<div>
			<table id="shipmentDetails">
				<tr>
					<td>物品名稱</td><td>數量</td><td></td>
					<s:iterator value="shipmentDetails">
						<tr>
							<td><s:property value="%{ productName }" /></td>
							<td><s:property value="%{ orderQuantity }" /></td>
							<td><input type='button' class='del' value='刪除' /></td>
						</tr>
					</s:iterator>
				</tr>
			</table>
		</div>
		<br />
		<input type="hidden" id="shipmentDetailXml" name="shipmentDetailXml" />
		<input 
			type="hidden" 
			id="shipmentCreateTime" 
			name="shipmentCreateTime" 
			value="<s:date name='shipment.shipmentCreateTime' format='yyyy/MM/dd HH:mm:ss' />" />
		<input type="hidden" id="shipment.status" name="shipment.status" value="${ shipment.status }" />
		<input type="button" onclick="validateForm()" value="儲存" />
		<input type="button" onclick="location.href='shipment';" value="取消" />
	</form>
</div>
</body>
</html>