<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Beltom - Shipment History Detail</title>
<link rel="stylesheet" type="text/css" href="css/shipment_query.css">
<link rel="stylesheet" type="text/css" href="css/align_center.css">
</head>
<body>
<h1><i>Beltom 百通科技</i></h1>
<div align="center" style="margin-top: 50px">
	<h2>出貨歷程查詢結果</h2>
	<hr />
	<table>
		<thead>
			<tr>
				<td>出貨名稱</td>
				<td>執行時間</td>
				<td>狀態</td>
			</tr>
		</thead>
		<tbody>
			<s:iterator value="shipmentHistories">
				<tr>
					<td><s:property value="shipment.shipmentName" /></td>
					<td><s:date name="createDate" format="yyyy/MM/dd HH:mm:ss" /></td>
					<td><s:property value="stateString" /></td>
				</tr>
			</s:iterator>
		</tbody>
	</table>
</div>
</body>
</html>