<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Beltom - Shipment History Query</title>
<script type="text/javascript" src="js/shipmentHistory_query.js"></script>
<link rel="stylesheet" type="text/css" href="css/shipment_query.css">
<link rel="stylesheet" type="text/css" href="css/align_center.css">
</head>
<body onload="pagination()">
<h1><i>Beltom 百通科技</i></h1>
<div align="center" style="margin-top: 50px">
	<h2>出貨查詢結果</h2>
	<hr />
	<table>
		<thead>
			<tr>
				<td>功能</td>
				<td>出貨名稱</td>
				<td>建立時間</td>
				<td>狀態</td>
			</tr>
		</thead>
		<tbody>
			<s:iterator value="shipments" id="sid">
				<tr>
					<td><s:a href="javascript: showHistory('%{ shipmentId }', '%{ shipmentName }')">歷程</s:a></td>
					<td><s:property value="shipmentName" /></td>
					<td><s:date name="shipmentCreateTime" format="yyyy/MM/dd HH:mm:ss" /></td>
					<td><s:property value="stateString" /></td>
				</tr>
			</s:iterator>
			<s:hidden 
				id="shipmentId" 
				name="shipment.shipmentId" 
				value="%{ shipment.shipmentId }" />
			<s:hidden 
				id="shipmentName" 
				name="shipment.shipmentName" 
				value="%{ shipment.shipmentName }" />
			<s:hidden 
				id="startTime" 
				name="startTime" 
				value="%{ startTime }" />
			<s:hidden 
				id="endTime" 
				name="endTime" 
				value="%{ endTime }" />
			<s:hidden 
				id="pageIndex" 
				name="pageIndex" 
				value="%{ pageIndex }" />
			<s:hidden 
				id="pageSize" 
				name="pageSize" 
				value="%{ pageSize }" />
			<s:hidden 
				id="totalRecords" 
				value="%{ totalRecords }" />
		</tbody>
	</table>
	<ul id="pages"></ul>
</div>
</body>
</html>