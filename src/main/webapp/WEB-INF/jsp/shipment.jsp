<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Beltom - Shipment</title>
<!-- 
<link rel="stylesheet" type="text/css" href="css/form.css">
 -->
<link href="jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" />
<link href="jquery-Timepicker/jquery-ui-timepicker-addon.min.css" rel="stylesheet" />
<script src="js/jquery-3.4.1.min.js"></script>
<script src="jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="jquery-ui-1.12.1/datepicker-zh-TW.js"></script>
<script src="jquery-Timepicker/jquery-ui-timepicker-addon.min.js"></script>
<script type="text/javascript" src="js/shipment.js"></script>
</head>
<body>
<h1><i>Beltom 百通科技</i></h1>
<h2><s:a href="shipmentHistory">出貨歷程查詢</s:a></h2>
<div>
	<h2>出貨排程查詢</h2>
	<form id="shipmentForm" method="post">
		<s:fielderror />
		<label for="shipment.shipmentId">出貨編號</label>
		<input type="text" id="shipment.shipmentId" name="shipment.shipmentId" />
		<label for="shipment.shipmentName">出貨名稱</label>
		<input type="text" id="shipment.shipmentName" name="shipment.shipmentName" />
		<label for="shipment.status">狀態</label>
		<s:select id="shipment.status"
			list="#{ '-1':'', '0':'草稿', '1':'暫停', '2':'可出貨', '3':'出貨中', '4':'出貨完成', '5':'已取消' }" 
			name="shipment.status" />
		<label for="startTime">建立時間</label>
		<input type="text" name="startTime" id="startTime" class="date-range" autocomplete="off" placeholder="起 yyyy/mm/dd" />
		<span id="errStartTime"></span><br/>
		<input type="text" name="endTime" id="endTime" class="date-range" autocomplete="off" placeholder="迄 yyyy/mm/dd" />
		<span id="errEndTime"></span>
		<input type="button" onclick="location.href='create';" value="建立" />
		<input type="button" onclick="queryShipment('shipmentQuery')" value="查詢" />
	</form>
</div>
</body>
</html>