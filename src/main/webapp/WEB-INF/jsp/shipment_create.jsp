<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Beltom - Shipment Create</title>
<link href="css/align_center.css" rel="stylesheet">
<link href="jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" />
<link href="jquery-Timepicker/jquery-ui-timepicker-addon.min.css" rel="stylesheet" />
<script src="js/jquery-3.4.1.min.js"></script>
<script src="jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="jquery-ui-1.12.1/datepicker-zh-TW.js"></script>
<script src="jquery-Timepicker/jquery-ui-timepicker-addon.min.js"></script>
<script type="text/javascript" src="js/shipment_create.js"></script>
</head>
<body>
<h1><i>Beltom 百通科技</i></h1>
<div align="center" style="margin-top: 50px">
	<h2>建立出貨作業</h2>
	<form action="shipmentCreate" method="post">
		<s:fielderror />
		<table>
			<tr>
				<td>出貨名稱</td>
				<td>
					<input type="text" id="shipment.shipmentName" name="shipment.shipmentName" required />
					<span id="errName"></span>
				</td>
				<td>預計出貨時間</td>
				<td>
					<input type="text"
							id="planShipmentTime"
							name="planShipmentTime" 
							class="date-range"
							autocomplete="off"
							placeholder="yyyy/MM/dd HH:mm:ss"
							required />
					<span id="errPlanTime"></span>
				</td>
			</tr>
			<tr>
				<td>出貨地點</td>
				<td>
					<input type="text" id="shipment.place" name="shipment.place" required />
					<span id="errPlace"></span>
				</td>
				<td>收件人</td>
				<td>
					<input type="text" id="shipment.receiver" name="shipment.receiver" required />
					<span id="errReceiver"></span>
				</td>
			</tr>
			<tr>
				<td>收件人電話</td>
				<td>
					<input type="text" id="shipment.phone" name="shipment.phone" required />
					<span id="errPhone"></span>
				</td>
				<td>出貨人員</td>
				<td>
					<input type="text" id="shipment.operator" name="shipment.operator" required />
					<span id="errOperator"></span>
				</td>
			</tr>
			<tr><td colspan="4">備註</td></tr>
			<tr>
				<td colspan="4">
					<textarea id="shipment.remark" name="shipment.remark"></textarea>
				</td>
			</tr>
		</table>
		<div>
			<span id="errShipmentDetails"></span>
			<table>
				<tr>
					<td>物品名稱</td><td>數量</td><td></td>
				</tr>
				<tr>
					<td><input type="text" id="name" /></td>
					<td><input type="number" id="qty" /></td>
					<td>
						<input id="addRow" type="button" value="新增" />
					</td>
				</tr>
			</table>
		</div>
		<div>
			<table id="shipmentDetails">
				<tr>
					<td>物品名稱</td><td>數量</td><td></td>
				</tr>
			</table>
		</div>
		<br />
		<input type="hidden" id="shipmentDetailXml" name="shipmentDetailXml" />
		<input type="button" onclick="validateForm()" value="建立" />
		<input type="button" onclick="location.href='shipment';" value="取消" />
	</form>
</div>
</body>
</html>