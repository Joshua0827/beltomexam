<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Beltom - Register</title>
<link rel="stylesheet" href="css/bootstrap.css">
<script src="js/bootstrap.min.js"></script>
<!-- 
<link rel="stylesheet" type="text/css" href="css/form.css">
 -->
<script src="js/register.js"></script>
</head>
<body>
<div class="container">
	<h1><i>Beltom 百通科技</i></h1>
</div>

<div>
	<h2>註冊</h2>
	<form action="register" method="post" onsubmit="return validateForm()">
		<s:fielderror />
		<label for="user.name">帳號</label>
		<input type="text" id="user.name" name="user.name" required />
		<span id="errName"></span><br/>
		<label for="user.password">密碼</label>
		<input type="password" id="user.password" name="user.password" required />
		<span id="errPassword"></span><br />
		<label for="passwordRepeat">重複密碼</label>
		<input type="password" id="passwordRepeat" name="passwordRepeat" required />
		<span id="errPasswordRepeat"></span>
		<input type="submit" value="提交" />
	</form>
	<s:a href="login.jsp" class="text-decoration-none"><i>返回登入</i></s:a>
</div>
</body>
</html>