function validateForm() {
	var form = document.forms[0];
	var name, planTime, place, receiver, phone, operator, shipmentDetails, isValid;

	name = document.getElementById("shipment.shipmentName").value;
	planTime = document.getElementById("planShipmentTime").value;
	place = document.getElementById("shipment.place").value;
	receiver = document.getElementById("shipment.receiver").value;
	phone = document.getElementById("shipment.phone").value;
	operator = document.getElementById("shipment.operator").value;

	if (name.trim() == "") {
		document.getElementById("errName").innerHTML = "請輸入出貨名稱";
		isValid = false;
	} else {
		document.getElementById("errName").innerHTML = "";
	}

	if (planTime.trim() == "") {
		document.getElementById("errPlanTime").innerHTML = "請輸入預計出貨時間";
		isValid = false;
	} else {
		document.getElementById("errPlanTime").innerHTML = "";
	}

	if (place.trim() == "") {
		document.getElementById("errPlace").innerHTML = "請輸入出貨地點";
		isValid = false;
	} else {
		document.getElementById("errPlace").innerHTML = "";
	}

	if (receiver.trim() == "") {
		document.getElementById("errReceiver").innerHTML = "請輸入收件人";
		isValid = false;
	} else {
		document.getElementById("errReceiver").innerHTML = "";
	}

	if (phone.trim() == "") {
		document.getElementById("errPhone").innerHTML = "請輸入收件人電話";
		isValid = false;
	} else {
		document.getElementById("errPhone").innerHTML = "";
	}

	if (operator.trim() == "") {
		document.getElementById("errOperator").innerHTML = "請輸入出貨人員";
		isValid = false;
	} else {
		document.getElementById("errOperator").innerHTML = "";
	}
	
	shipmentDetails = createShipmentDetails();
	
	if (shipmentDetails.indexOf("<product>") < 0) {
		document.getElementById("errShipmentDetails").innerHTML = "請輸入出貨清單";
		isValid = false;
	} else {
		document.getElementById("errShipmentDetails").innerHTML = "";
		document.getElementById("shipmentDetailXml").value = shipmentDetails;
	}
	
	if (isValid == false) {
		return false;
	}
	
	form.submit();
}

function createShipmentDetails() {
	var detail = "<shipmentDetail>";
	
	$("#shipmentDetails tr:not(:first)").each(function() {
		var name = $(this).find("td:eq(0)").text();
		var qty = $(this).find("td:eq(1)").text();
		
		detail += 
			"<product>" +
			"<name>" + name + "</name>" + 
			"<qty>" + qty + "</qty></product>";
	});
	
	detail += "</shipmentDetail>";
	
	return detail;
}

$(function() {
	$("#shipmentDetails").on("click", ".del", function() {
		$(this).closest("tr").remove();
	});
	
	$("#addRow").on("click", function() {
		var name = $("#name").val().trim(), qty = $("#qty").val().trim();
		var delButton = "<input type='button' class='del' value='刪除' />";
		
		if(!name || !$.isNumeric(qty)) return false;
		
		$("#shipmentDetails").append("<tr>"
				+ "<td>" + name + "</td>"
				+ "<td>" + qty + "</td>"
				+ "<td>" + delButton + "</td></tr>");
		
		$("#name").val("");
		$("#qty").val("");
	});
	
	$("input.date-range").datetimepicker({
		"timeText": "時間",
		"hourText": "時",
		"minuteText": "分",
		"secondText": "秒",
		"currentText": "現在",
		"closeText": "完成",
		"dateFormat": "yy/mm/dd",
		"timeFormat": "HH:mm:ss"
	});
});
