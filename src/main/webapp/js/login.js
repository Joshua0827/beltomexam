function validateForm() {
	var name, password, isValid;

	name = document.getElementById("user.userName").value;
	password = document.getElementById("user.userPassword").value;

	if (name.trim() == "") {
		document.getElementById("errName").innerHTML = "請輸入帳號";
		isValid = false;
	} else {
		document.getElementById("errName").innerHTML = "";
	}

	if (password.trim() == "") {
		document.getElementById("errPassword").innerHTML = "請輸入密碼";
		isValid = false;
	} else {
		document.getElementById("errPassword").innerHTML = "";
	}
	
	if(isValid == false) {
		return false;
	}
}