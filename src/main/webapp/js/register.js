function validateForm() {
	var name, password, passwordRepeat, isValid;

	name = document.getElementById("user.userName").value;
	password = document.getElementById("user.userPassword").value;
	passwordRepeat = document.getElementById("passwordRepeat").value;

	if (name.trim() == "") {
		document.getElementById("errName").innerHTML = "請輸入帳號";
		isValid = false;
	} else {
		document.getElementById("errName").innerHTML = "";
	}

	if (password.trim() == "") {
		document.getElementById("errPassword").innerHTML = "請輸入密碼";
		isValid = false;
	} else {
		document.getElementById("errPassword").innerHTML = "";
	}
	
	if (passwordRepeat.trim() == "") {
		document.getElementById("errPasswordRepeat").innerHTML = "請重複密碼";
		isValid = false;
	} else {
		document.getElementById("errPasswordRepeat").innerHTML = "";
	}
	
	return isValid;
}