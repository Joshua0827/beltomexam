function queryShipment(action) {
	var form = document.getElementById("shipmentForm");
	var hiddenIndex = document.createElement("input");
	var hiddenSize = document.createElement("input");
	
	hiddenIndex.setAttribute("type", "hidden");
	hiddenIndex.setAttribute("name", "pageIndex");
	hiddenIndex.setAttribute("value", "0");
	
	hiddenSize.setAttribute("type", "hidden");
	hiddenSize.setAttribute("name", "pageSize");
	hiddenSize.setAttribute("value", "8");
	
	form.appendChild(hiddenIndex);
	form.appendChild(hiddenSize);
	
	form.action = action;
	form.submit();
}

$(function() {
	$("input.date-range").datepicker({
		"currentText": "現在",
		"closeText": "完成",
		"dateFormat": "yy/mm/dd",
		"timeFormat": "HH:mm:ss"
	});
});