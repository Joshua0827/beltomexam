// 分頁功能
function pagination() {
	var pageIndex = parseInt(document.getElementById("pageIndex").value);
	var pageSize = parseInt(document.getElementById("pageSize").value);
	var totalRecords = parseInt(document.getElementById("totalRecords").value);
	var totalPages = Math.ceil(totalRecords / pageSize);
	var pages = document.getElementById("pages");
	
	if(totalPages == 0) return;
	
	if(pageIndex == 0) {
		addPaginationTextNode(pages, "上一頁");
	} else {
		addPaginationLinks(pages, "上一頁", (pageIndex - 1));
	}
	
	for(var i = 0; i < totalPages; i++) {
		if(i == pageIndex) {
			addPaginationTextNode(pages, (i + 1));
		} else {
			addPaginationLinks(pages, (i + 1), i);
		}
	}
	
	if((pageIndex + 1) == totalPages) {
		addPaginationTextNode(pages, "下一頁");
	} else {
		addPaginationLinks(pages, "下一頁", (pageIndex + 1));
	}
}

//分頁文字節點
function addPaginationTextNode(pages, nodeName) {
	var listNode = document.createElement("li");
	var txtNode = document.createTextNode(nodeName);
	
	listNode.appendChild(txtNode);
	pages.appendChild(listNode);
}

//分頁連結節點
function addPaginationLinks(pages, linkName, linkIndex) {
	var listNode = document.createElement("li");
	var linkNode = document.createElement("a");
	
	linkNode.setAttribute("href", "javascript: queryShipment(" + linkIndex + ")");
	linkNode.innerHTML = linkName;
	listNode.appendChild(linkNode);
	pages.appendChild(listNode);
}

// 依照分頁index查詢訊息
function queryShipment(pageIndex) {
	var form = document.createElement("form");
	var hiddenIndex = document.createElement("input");
	var hiddenSubmit = document.createElement("input");
	
	form.appendChild(document.getElementById("shipmentId"));
	form.appendChild(document.getElementById("shipmentName"));
	form.appendChild(document.getElementById("status"));
	form.appendChild(document.getElementById("startTime"));
	form.appendChild(document.getElementById("endTime"));
	form.appendChild(document.getElementById("pageSize"));
	
	hiddenIndex.setAttribute("type", "hidden");
	hiddenIndex.setAttribute("name", "pageIndex");
	hiddenIndex.setAttribute("value", pageIndex);
	form.appendChild(hiddenIndex);
	
	hiddenSubmit.setAttribute("type", "submit");
	hiddenSubmit.setAttribute("style", "display:none;");
	form.appendChild(hiddenSubmit);
	
	form.setAttribute("action", "shipmentQuery");
	form.setAttribute("method", "post");
	
	document.body.appendChild(form);
	form.submit();
}

//顯示出貨單內容
function showShipmentDetail(shipmentId) {
	var form = document.createElement("form");
	var hiddenId = document.createElement("input");
	var hiddenSubmit = document.createElement("input");
	
	hiddenId.setAttribute("type", "hidden");
	hiddenId.setAttribute("name", "shipment.shipmentId");
	hiddenId.setAttribute("value", shipmentId);
	form.appendChild(hiddenId);
	
	hiddenSubmit.setAttribute("type", "submit");
	hiddenSubmit.setAttribute("style", "display:none;");
	form.appendChild(hiddenSubmit);
	
	form.setAttribute("action", "shipmentDetail");
	form.setAttribute("method", "post");
	
	document.body.appendChild(form);
	form.submit();
}

//修改出貨明細
function updateShipment(shipmentId) {
	var form = document.createElement("form");
	var hiddenId = document.createElement("input");
	var hiddenSubmit = document.createElement("input");
	
	hiddenId.setAttribute("type", "hidden");
	hiddenId.setAttribute("name", "shipment.shipmentId");
	hiddenId.setAttribute("value", shipmentId);
	form.appendChild(hiddenId);
	
	hiddenSubmit.setAttribute("type", "submit");
	hiddenSubmit.setAttribute("style", "display:none;");
	form.appendChild(hiddenSubmit);
	
	form.setAttribute("action", "update");
	form.setAttribute("method", "post");
	
	document.body.appendChild(form);
	form.submit();
}

//出貨
function startShipment(shipmentId) {
	var form = document.createElement("form");
	var hiddenId = document.createElement("input");
	var hiddenSubmit = document.createElement("input");
	
	hiddenId.setAttribute("type", "hidden");
	hiddenId.setAttribute("name", "shipment.shipmentId");
	hiddenId.setAttribute("value", shipmentId);
	form.appendChild(hiddenId);
	
	hiddenSubmit.setAttribute("type", "submit");
	hiddenSubmit.setAttribute("style", "display:none;");
	form.appendChild(hiddenSubmit);
	
	form.setAttribute("action", "start");
	form.setAttribute("method", "post");
	
	document.body.appendChild(form);
	form.submit();
}

//暫停
function pauseShipment(shipmentId) {
	var form = document.createElement("form");
	var hiddenId = document.createElement("input");
	var hiddenSubmit = document.createElement("input");
	
	hiddenId.setAttribute("type", "hidden");
	hiddenId.setAttribute("name", "shipment.shipmentId");
	hiddenId.setAttribute("value", shipmentId);
	form.appendChild(hiddenId);
	
	hiddenSubmit.setAttribute("type", "submit");
	hiddenSubmit.setAttribute("style", "display:none;");
	form.appendChild(hiddenSubmit);
	
	form.setAttribute("action", "pause");
	form.setAttribute("method", "post");
	
	document.body.appendChild(form);
	form.submit();
}

//取消
function cancelShipment(shipmentId) {
	var form = document.createElement("form");
	var hiddenId = document.createElement("input");
	var hiddenSubmit = document.createElement("input");
	
	hiddenId.setAttribute("type", "hidden");
	hiddenId.setAttribute("name", "shipment.shipmentId");
	hiddenId.setAttribute("value", shipmentId);
	form.appendChild(hiddenId);
	
	hiddenSubmit.setAttribute("type", "submit");
	hiddenSubmit.setAttribute("style", "display:none;");
	form.appendChild(hiddenSubmit);
	
	form.setAttribute("action", "cancel");
	form.setAttribute("method", "post");
	
	document.body.appendChild(form);
	form.submit();
}