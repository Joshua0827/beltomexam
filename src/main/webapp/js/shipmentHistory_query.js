// 分頁功能
function pagination() {
	var pageIndex = parseInt(document.getElementById("pageIndex").value);
	var pageSize = parseInt(document.getElementById("pageSize").value);
	var totalRecords = parseInt(document.getElementById("totalRecords").value);
	var totalPages = Math.ceil(totalRecords / pageSize);
	var pages = document.getElementById("pages");
	
	if(totalPages == 0) return;
	
	if(pageIndex == 0) {
		addPaginationTextNode(pages, "上一頁");
	} else {
		addPaginationLinks(pages, "上一頁", (pageIndex - 1));
	}
	
	for(var i = 0; i < totalPages; i++) {
		if(i == pageIndex) {
			addPaginationTextNode(pages, (i + 1));
		} else {
			addPaginationLinks(pages, (i + 1), i);
		}
	}
	
	if((pageIndex + 1) == totalPages) {
		addPaginationTextNode(pages, "下一頁");
	} else {
		addPaginationLinks(pages, "下一頁", (pageIndex + 1));
	}
}

function addPaginationTextNode(pages, nodeName) {
	var listNode = document.createElement("li");
	var txtNode = document.createTextNode(nodeName);
	
	listNode.appendChild(txtNode);
	pages.appendChild(listNode);
}

function addPaginationLinks(pages, linkName, linkIndex) {
	var listNode = document.createElement("li");
	var linkNode = document.createElement("a");
	
	linkNode.setAttribute("href", "javascript: queryShipment(" + linkIndex + ")");
	linkNode.innerHTML = linkName;
	listNode.appendChild(linkNode);
	pages.appendChild(listNode);
}

// 依照分頁index查詢訊息
function queryShipment(pageIndex) {
	var form = document.createElement("form");
	var hiddenIndex = document.createElement("input");
	var hiddenSubmit = document.createElement("input");
	
	form.appendChild(document.getElementById("shipmentName"));
	form.appendChild(document.getElementById("startTime"));
	form.appendChild(document.getElementById("endTime"));
	form.appendChild(document.getElementById("pageSize"));
	
	hiddenIndex.setAttribute("type", "hidden");
	hiddenIndex.setAttribute("name", "pageIndex");
	hiddenIndex.setAttribute("value", pageIndex);
	form.appendChild(hiddenIndex);
	
	hiddenSubmit.setAttribute("type", "submit");
	hiddenSubmit.setAttribute("style", "display:none;");
	form.appendChild(hiddenSubmit);
	
	form.setAttribute("action", "shipmentHistoryQuery");
	form.setAttribute("method", "post");
	
	document.body.appendChild(form);
	form.submit();
}

function showHistory(shipmentId, shipmentName) {
	var form = document.createElement("form");
	var hiddenId = document.createElement("input");
	var hiddenName = document.createElement("input");
	var hiddenSubmit = document.createElement("input");
	
	hiddenId.setAttribute("type", "hidden");
	hiddenId.setAttribute("name", "shipment.shipmentId");
	hiddenId.setAttribute("value", shipmentId);
	form.appendChild(hiddenId);
	
	hiddenName.setAttribute("type", "hidden");
	hiddenName.setAttribute("name", "shipment.shipmentName");
	hiddenName.setAttribute("value", shipmentName);
	form.appendChild(hiddenName);
	
	hiddenSubmit.setAttribute("type", "submit");
	hiddenSubmit.setAttribute("style", "display:none;");
	form.appendChild(hiddenSubmit);
	
	form.setAttribute("action", "shipmentHistoryDetail");
	form.setAttribute("method", "post");
	
	document.body.appendChild(form);
	form.submit();
}